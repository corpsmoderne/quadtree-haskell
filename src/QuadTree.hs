module QuadTree where

maxElems = 5

data Point = Point { px :: Float
                   , py :: Float }
  
instance Show Point where
  show (Point px py) = "/"++show px++","++show py++"/"

class HasPosition a where
  getPos :: a -> Point
  getx :: a -> Float
  gety :: a -> Float

instance HasPosition Point where
  getPos p = p
  getx p = px p
  gety p = py p

data Rect = Rect { rx :: Float
                 , ry :: Float
                 , rw :: Float
                 , rh :: Float }

class Box a where
  getBox :: a -> Rect
  contains :: HasPosition b => a -> b -> Bool
  overlaps :: Box b => a -> b -> Bool

instance Box Rect where
  getBox r = r
  contains (Rect rx ry rw rh) obj
    | rx <= px && rx + rw > px && ry <= py && ry + rh > py = True
    | otherwise = False
    where (Point px py) = getPos obj
  overlaps poly1 poly2
    | x1 > x2+w2 || x1+w1 < x2 || y1 > y2+h2 || y1+h1 < y2 = False
    | otherwise = True
    where (Rect x1 y1 w1 h1) = getBox poly1
          (Rect x2 y2 w2 h2) = getBox poly2
    
instance Show Rect where
  show (Rect rx ry rw rh) = "|" ++ show rx ++ "," ++ show ry ++ 
                            " " ++ show rw ++ "," ++ show rh ++ "|"

data QuadTree a = Leaf { elems :: [a]
                       , box :: Rect }
                | Quad { quads :: [QuadTree a]
                       , box :: Rect } deriving Show

instance Box (QuadTree a) where
  getBox q = box q
  contains q = contains $ box q
  overlaps q1 q2 = overlaps (getBox q1) (getBox q2)
  
addInQuad :: HasPosition a => QuadTree a -> a -> QuadTree a
addInQuad (Quad quads boxq) p = Quad newQuads boxq
  where newQuads = map dispatch quads
        dispatch q | q `contains` p = addInQuad q p
                   | otherwise = q
addInQuad (Leaf lst boxq) p | length lst < maxElems = Leaf (p:lst) boxq
                            | otherwise = splitQuad (Leaf (p:lst) boxq)

splitQuad :: HasPosition a => QuadTree a -> QuadTree a
splitQuad (Leaf lst box@(Rect x y w h)) = Quad leafs box
  where boxes = [ Rect x y (w/2) (h/2),
                  Rect (x+w/2) y (w/2) (h/2),
                  Rect x (y+h/2) (w/2) (h/2),
                  Rect (x+w/2) (y+h/2) (w/2) (h/2) ]
        leafs = map (\ bx -> Leaf (filter (contains bx) lst) bx) boxes

query :: HasPosition a => Rect -> QuadTree a -> [a]
query rect (Leaf lst box) = filter (contains rect) lst
query rect (Quad quads boxq) = foldl (++) [] pts
  where qq = filter (overlaps rect) quads
        pts = map (query rect) qq
