module Main where

import SFML.Audio
import SFML.Graphics
import SFML.Window

import System.Environment
import System.Random
import Control.Applicative
import QuadTree

winWidth  = 800
winHeight = 800

data Boid = Boid { id :: Int
                 , pos :: Point } deriving Show

instance HasPosition Boid where
  getPos b = pos b
  getx b = px $ pos b
  gety b = py $ pos b

data State = State { query :: QuadTree.Rect
                   , quad  :: QuadTree Boid
                   , boidGen :: [Boid]
                   , boidToGen :: Int } deriving Show

main :: IO ()
main = startApp . getOps =<< getArgs

getOps :: [String] -> Int
getOps [] = 0
getOps [n] = read n

startApp :: Int -> IO ()
startApp n = do
  wnd <- createRenderWindow (VideoMode winWidth winHeight 32)
    "SFML Haskell Demo" [SFDefaultStyle] ctxSettings
  startState <- state <$> newStdGen
  mainLoop wnd startState
  destroy wnd
  where ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
        queryRect = QuadTree.Rect (-200) (-200) 100 100
        quad = Leaf [] (Rect 0 0 (fromIntegral winWidth)
                                 (fromIntegral winHeight))
        state gen = State queryRect quad (boidsGen gen) n
        boidsGen gen = genBoids 0 (randomRs (1,winWidth-1) gen)

genBoids :: Int -> [Int] -> [Boid]
genBoids n (x:y:xs) = Boid n pos : genBoids (n+1) xs
  where pos = Point (fromIntegral x) (fromIntegral y)

mainLoop :: RenderWindow -> State -> IO ()
mainLoop wnd state@(State query quad bGen bN) =
  drawFrame >> waitOrPoll bN >>= doEvents wnd state >>= nextLoop
  where waitOrPoll 0 = waitEvent wnd
        waitOrPoll _ = pollEvent wnd
        nextLoop (Just newState) = mainLoop wnd newState
        nextLoop Nothing = return ()
        drawFrame = clearRenderWindow wnd black >>
                    createRectangleShape >>=
                    (\(Right rct) -> drawQuad wnd rct quad >> destroy rct) >>
                    drawQuery wnd query quad >> display wnd

doEvents :: RenderWindow -> State -> Maybe SFEvent -> IO (Maybe State)
doEvents wnd state@(State _ _ _ 0) Nothing = return (Just state)
doEvents wnd state Nothing = addRandomBoid wnd state
doEvents wnd _ (Just SFEvtClosed) = return Nothing
doEvents wnd state (Just event) = pollEvent wnd >>= doEvents wnd newState
  where newState = updateState state event

updateState :: State -> SFEvent -> State
updateState (State query quad bGen bN) (SFEvtMouseButtonPressed btn x y) =
  State query (newQuadTree x y) bGen bN
  where newQuadTree x y = addInQuad quad $ newPoint x y
        newPoint x y = Boid 0 $ Point (fromIntegral x) (fromIntegral y)
updateState (State query quad bGen bN) (SFEvtMouseMoved x y) =
  State (newQuery x y) quad bGen bN
  where newQuery x y = QuadTree.Rect
          ((fromIntegral x)-(rw query)/2)
          ((fromIntegral y)-(rh query)/2)
          (rw query) (rh query)
updateState state _ = state      
          
drawRect :: RenderWindow -> RectangleShape -> QuadTree.Rect -> IO ()
drawRect rw rect (QuadTree.Rect x y w h) =
  SFML.Graphics.setPosition rect (Vec2f x y) >>
  setSize rect (Vec2f w h) >>
  setFillColor rect (Color 0 0 0 0) >>
  setOutlineColor rect (Color 0 0 255 255) >>
  setOutlineThickness rect 0.5 >>
  drawRectangle rw rect Nothing >>
  return ()
  
drawPoints :: RenderWindow -> RectangleShape -> [Point] -> IO ()
drawPoints rw rect boids = setSize rect (Vec2f 2 2) >>
                           setOutlineColor rect (Color 0 0 0 0) >>
                           setOutlineThickness rect 0 >>
                           mapM drawP boids >> return ()
  where drawP (Point x y) =
          SFML.Graphics.setPosition rect (Vec2f (x-1) (y-1)) >>
          drawRectangle rw rect Nothing
  
drawQuad :: RenderWindow -> RectangleShape -> QuadTree Boid -> IO ()
drawQuad rw rect (Leaf lst box) = setFillColor rect (Color 255 0 0 255) >>
                                  drawPoints rw rect (map pos lst) >>
                                  drawRect rw rect box >>
                                  return ()
drawQuad rw rect (Quad quads box) = mapM (drawQuad rw rect) quads >>
                                    drawRect rw rect box >>
                                    return ()

drawQuery :: RenderWindow -> QuadTree.Rect -> QuadTree Boid -> IO ()
drawQuery wnd rectQuery@(Rect rx ry rw rh) quad =
  createRectangleShape >>= drawQ >>= drawPts >>= destroy
  where drawQ (Right rect) = SFML.Graphics.setPosition rect (Vec2f rx ry) >>
          setSize rect (Vec2f rw rh) >>
          setFillColor rect (Color 0 0 0 0) >>
          setOutlineColor rect (Color 255 0 0 255) >>
          setOutlineThickness rect 2 >>
          drawRectangle wnd rect Nothing >>
          return rect
        drawPts rect = setSize rect (Vec2f 2 2) >>
          setFillColor rect (Color 255 255 0 255) >>
          setOutlineColor rect (Color 0 0 0 0) >>
          setOutlineThickness rect 0 >>
          (drawPoints wnd rect $ map pos $ QuadTree.query rectQuery quad) >>
          return rect

addRandomBoid :: RenderWindow -> State -> IO (Maybe State)
addRandomBoid wnd state@(State _ _ _ 0) = return $ Just state
addRandomBoid wnd (State query quad bGen bN) =
  return $ Just (State query newQuad newBGen (bN - 10)) 
  where (boids, newBGen) = splitAt (min bN 10) bGen
        newQuad = foldl addInQuad quad boids
